import Vue from 'vue'
import {
  Table,
  TableColumn,
  Button,
  ButtonGroup,
  Pagination,
  Dialog,
  Form,
  FormItem,
  Input,
  Radio,
  DatePicker,
  RadioGroup,
  Message
} from 'element-ui'

Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Button)
Vue.use(ButtonGroup)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(DatePicker)

Vue.prototype.$message = Message
